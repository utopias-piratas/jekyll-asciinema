---
layout: post
author: fauno
title: Actualizar Signal en el repositorio F-Droid
cast: fdroid.cast
---

El repositorio F-Droid se encuentra en
<https://0xacab.org/partido-interdimensional-pirata/fdroid-repo>, las
partes privadas hay que pedírselas a alguien, lo mismo que acceso al
servidor.

Hay que instalar el paquete `fdroidserver` en la versión 1.0.6.
